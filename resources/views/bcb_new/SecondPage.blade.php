<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="./css/bootstrap.css">
    <link rel="stylesheet" href="./css/style.css">

    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- this file was missing -->
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

<!-- this file was moved after the jQuery Datatables library was laoded -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>

<script>
  function loadPage() {
  var toastElList = [].slice.call(document.querySelectorAll('.toast'))
  var toastList = toastElList.map(function (toastEl) {
  return new bootstrap.Toast(toastEl, option)
  }
})
</script>

</head>
<body onload="loadPage()" id="toastbtn">  
  <header>
    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow-sm p-0 mb-5 bg-white rounded">
        <div class="container-fluid">
          <a class="navbar-brand" href="/">
            <img src  ="./img/svg/logo.svg" alt="BCB" class="header_logo"/>
          </a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            
            
            <ul class="navbar-nav me-auto mb-0 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">О проекте</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">Сотрудничество</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">Товары и услуги</a>
              </li>
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">Что Вы получите</a>
              </li>
              
            </ul>

            <form class="d-flex">

              
              <input class="form-control col-3" type="search" placeholder="Поиск"  aria-label="Search">  
                

              <img class="vector_logo" src="./img/svg/Vector.svg">

              <div class="dropdown">
                <a href="/" class="union_logo-link" class="btn btn-danger dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" title="Tooltip on bottom">
                  <img src  ="./img/svg/Union.svg" alt="Union" class="union_logo" />
                </a>
                <ul class="dropdown-menu  " aria-labelledby="dropdownMenuButton1" aria-expanded="false" >
                  <li><a class="dropdown-item" href="#"> <img src  ="./img/svg/1.svg" alt="1" class="logo_1"/> Личный кабинет</a></li>
                  <li><a class="dropdown-item" href="#"> <img src  ="./img/svg/2.svg" alt="2" class="logo_2"/> Мои покупки</a></li>
                  <li><a class="dropdown-item" href="#"> <img src  ="./img/svg/3.svg" alt="3" class="logo_3"/> Договор с bcb</a></li>
                  <li><a class="dropdown-item" href="#"> <img src  ="./img/svg/4.svg" alt="4" class="logo_4"/> Акты</a></li>
                  <li><a class="dropdown-item" href="#"> <img src  ="./img/svg/5.svg" alt="5" class="logo_5"/> Выплаты</a></li>
                  <li><a class="dropdown-item" href="#"> <img src  ="./img/svg/6.svg" alt="6" class="logo_6"/> Новости</a></li>
                  <li><a class="dropdown-item" href="#"> <img src  ="./img/svg/7.svg" alt="7" class="logo_7"/> Отзывы</a></li>
                  <li><a class="dropdown-item" href="#"> <img src  ="./img/svg/8.svg" alt="8" class="logo_8"/> Чат</a></li>
                </ul> 

            </form>

            </div>
          </div>
        </div>
      </nav>
  </header>

  <main class="second_page_main">

  

</div>

    <div class="toast hide align-items-center text-white bg-warning border-0 fade showing" id="liveToast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true">
      <div class="d-flex">
        <div class="toast-body">
          Зарегистрируйтесь, чтобы получить скидку
        </div>
      </div>
    </div>

    <div class="rcorners">
    <div class="container">
      <div class="container ">
        <div class="col col-lg-12 col-md-12 col-12 ">
            <p class="get_discount_label">КАК ВЫ МОЖЕТЕ ПОЛУЧИТЬ СКИДКУ</p>

            <input class="catalog_search" type="search" placeholder="Найти в каталоге" aria-label="Search">
            <img class="catalog_vector_logo" src="./img/svg/Catalog_vector.svg">
          </div>
        </div>

        <div class="row justify-content-center">

          <div class="col-lg-10">
            <div class="card-group my-6 mb-0">
  
              <div class="card" id="first_cards">
                <img class="card-img-top" src="./img/svg/card0.svg" alt="Card image cap">
              </div>
  
              <div class="card" id="first_cards">
                <img class="card-img-top" src="./img/svg/card1.svg" alt="Card image cap">
              </div>
  
              <div class="card" id="first_cards">
                <img class="card-img-top" src="./img/svg/card2.svg" alt="Card image cap">
              </div>
  
              <div class="card" id="first_cards">
                <img class="card-img-top" src="./img/svg/card3.svg" alt="Card image cap">
              </div>
  
              <div class="card" id="first_cards">
                <img class="card-img-top" src="./img/svg/card4.svg" alt="Card image cap">
              </div>
  
              <div class="card" id="first_cards">
                <img class="card-img-top" src="./img/svg/card5.svg" alt="Card image cap">
              </div>
            </div>
          </div>

          <div class="col-lg-12">
            <p class="popular_org_label">
              ПОПУЛЯРНЫЕ ОРГАНИЗАЦИИ
            </p>
          </div>

          <div class="col-lg-10">
          <div class="row row-cols-1 row-cols-md-5 g-4">

              <div class="col">
                <div class="card h-100">
                <p class="card-text" id="card_label"><span class="text-muted">Клинкерная плитка FELDHAUS KLINKER</span></p>
                  <div class="card-body">
                    <img src="./img/svg/logo1.svg" class="card-img-top" id="popular_comp_logo1" alt="..."> 
                    <button type="button" class="btn btn-outline-secondary" >Подробнее</button>
                  </div>
                </div>
              </div>

              <div class="col">
                <div class="card h-100">
                <p class="card-text" id="card_label"><span class="text-muted">Кирпич и плитка VANDERSANDEN</span></p>
                  <div class="card-body">
                    <img src="./img/svg/logo2.svg" class="card-img-top" id="popular_comp_logo2" alt="..."> 
                    <button type="button" class="btn btn-outline-secondary" >Подробнее</button>
                  </div>
                </div>
              </div>

              <div class="col">
                <div class="card h-100">
                <p class="card-text" id="card_label"><span class="text-muted">Керамическая черепица CREATON</span></p>
                  <div class="card-body">
                    <img src="./img/svg/logo3.svg" class="card-img-top" id="popular_comp_logo3" alt="..."> 
                    <button type="button" class="btn btn-outline-secondary" >Подробнее</button>
                  </div>
                </div>
              </div>

              <div class="col">
                <div class="card h-100">
                <p class="card-text" id="card_label"><span class="text-muted">Фальцевая кровля</span></p>
                  <div class="card-body">
                    <img src="./img/svg/logo4.svg" class="card-img-top" id="popular_comp_logo4" alt="..."> 
                    <button type="button" class="btn btn-outline-secondary" >Подробнее</button>
                  </div>
                </div>
              </div>

              <div class="col">
                <div class="card h-100">
                <p class="card-text" id="card_label"><span class="text-muted">Водосточные системы RHEINZINK</span></p>
                  <div class="card-body">
                    <img src="./img/svg/logo5.svg" class="card-img-top" id="popular_comp_logo5" alt="..."> 
                    <button type="button" class="btn btn-outline-secondary" >Подробнее</button>
                  </div>
                </div>
              </div>
              

            </div>
          </div>

        </div>

      </div>
    </div>

    </div>

  </main>
    
  <footer class="text-center text-white" style="background-color: white;">
  
  <div class="container p-4">
    <section class="">
      <div class="row">
        <div class="col-lg-2 col-md-12 mb-4 mb-md-0 ">
          <div
            class="bg-image hover-overlay ripple shadow-1-strong rounded"
            data-ripple-color="light"
          >
          <a href="/" class="down_logo-link">
                <img src  ="./img/svg/logo.svg" alt="BCB" class="down_logo-link"/>
            </a>
            <a href="#!">
              <div
                class="mask"
                style="background-color: rgba(251, 251, 251, 0.2);"
              ></div>
            </a>
          </div>
        </div>
        
        <div class="col-lg-4 col-md-10 mb-4 mb-md-0 offset-lg-6 ">
          <div
            class="bg-image hover-overlay ripple shadow-1-strong rounded"
            data-ripple-color="light"
          >

          <p class="social_media_label">Мы в социальных сетях: <a href="#!" class="inst-link"><img src  ="./img/svg/inst.svg" width="35" alt="inst" class="inst-link"/></a> <a href="#!" class="fb-link"><img src  ="./img/svg/fb.svg" width="35" alt="fb" class="fb-link"/></a> <a href="#!" class="vk-link"><img src  ="./img/svg/vk.svg" width="35" alt="vk" class="vk-link"/></a></p>

            <a href="#!">
              <div
                class="mask"
                style="background-color: rgba(251, 251, 251, 0.2);"
              ></div>
            </a>
          </div>
        </div>
      </div>
    </section>
  </div>
  
  </footer>

 

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="./js/bootstrap.min.js"></script>


  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js"></script>
  <script>
    document.getElementById("toastbtn").onload= function() {
      var toastElList = [].slice.call(document.querySelectorAll('.toast'))
      var toastList = toastElList.map(function(toastEl) {
      // Creates an array of toasts (it only initializes them)
        return new bootstrap.Toast(toastEl) // No need for options; use the default options
      });
     toastList.forEach(toast => toast.show()); // This show them
      
      console.log(toastList); // Testing to see if it works
    };

  </script>

</body>
</html>
